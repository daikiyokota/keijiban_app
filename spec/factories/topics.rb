FactoryBot.define do
  factory :topic do
    title { "MyString" }
    association :user
  end
end
