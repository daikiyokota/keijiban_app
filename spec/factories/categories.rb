FactoryBot.define do
  factory :category do
    name { "MyString" }
    association :user
    association :topic
  end
end
