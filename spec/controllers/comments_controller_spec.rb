require 'rails_helper'

RSpec.describe CommentsController, type: :controller do
  describe 'POST #create' do
    let(:user) { FactoryBot.create(:user) }
    let(:topic) { FactoryBot.create(:topic) }
    let(:comment_attributes) do
      {
        content: 'テストです',
        user_id: user.id,
        topic_id: topic.id,
      }
    end

    before do
      login_user(user)
    end

    it 'saves new comment' do
      expect do
        post :create, params: { comment: comment_attributes }
      end.to change(Comment, :count).by(1)
    end

    it 'redirects the :create template' do
      post :create, params: { comment: comment_attributes }
      expect(response).to redirect_to(topic_path(topic))
    end
  end

  describe 'DELETE #destroy' do
    let!(:comment) { FactoryBot.create(:comment) }

    before do
      login_user(comment.user)
    end

    it 'deletes the comment' do
      expect do
        delete :destroy, params: { id: comment.id }
      end.to change(Comment, :count).by(-1)
    end

    it 'redirects topics/show' do
      delete :destroy, params: { id: comment.id }
      expect(response).to redirect_to(topic_path(comment.topic))
    end
  end
end
