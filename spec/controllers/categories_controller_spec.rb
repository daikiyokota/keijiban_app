require 'rails_helper'

RSpec.describe CategoriesController, type: :controller do
  describe 'POST #create' do
    let(:user) { FactoryBot.create(:user) }
    let(:topic) { FactoryBot.create(:topic, user: user) }
    let(:category_attributes) do
      {
        name: 'テストです',
        user_id: user.id,
        topic_id: topic.id,
      }
    end

    before do
      login_user(user)
    end

    it 'saves new category' do
      expect do
        post :create, params: { category: category_attributes }
      end.to change(Category, :count).by(1)
    end

    it 'redirects the topics#show' do
      post :create, params: { category: category_attributes }
      expect(response).to redirect_to(topic_path(topic))
    end
  end

  describe 'DELETE #destroy' do
    let(:user) { FactoryBot.create(:user) }
    let(:topic) { FactoryBot.create(:topic, user: user) }
    let!(:category) { FactoryBot.create(:category, user: user, topic: topic) }

    before do
      login_user(user)
    end

    it 'deletes the category' do
      expect do
        delete :destroy, params: { id: category.id }
      end.to change(Category, :count).by(-1)
    end

    it 'redirects topics/show' do
      delete :destroy, params: { id: category.id }
      expect(response).to redirect_to(topic_path(topic))
    end
  end
end
