require 'rails_helper'

RSpec.describe TopicsController, type: :controller do
  describe 'GET #index' do
    let(:topic) { FactoryBot.create(:topic) }
    let(:another_topic) { FactoryBot.create(:topic, title: '2つ目のスレッド') }
    let(:topics) { [topic, another_topic] }

    before { get :index }

    it 'has a 200 status code' do
      expect(response).to have_http_status(:ok)
    end

    it 'assigns @topics' do
      expect(assigns(:topics)).to match_array topics
    end

    it 'renders the :index template' do
      expect(response).to render_template :index
    end
  end

  describe "GET #new" do
    let(:user) { FactoryBot.create(:user) }

    before do
      login_user(user)
      get :new
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it 'assigns new @topic' do
      expect(assigns(:topic)).to be_a_new Topic
    end

    it 'renders the :new template' do
      expect(response).to render_template :new
    end
  end

  describe 'POST #create' do
    let(:user) { FactoryBot.create(:user) }
    let(:topic_attributes) do
      {
        title: 'テストです',
      }
    end

    before do
      login_user(user)
    end

    it 'saves new topic' do
      expect do
        post :create, params: { topic: topic_attributes }
      end.to change(Topic, :count).by(1)
    end

    it 'redirects the :create template' do
      post :create, params: { topic: topic_attributes }
      topic = Topic.last
      expect(response).to redirect_to(topic_path(topic))
    end
  end

  describe 'GET #show' do
    let(:topic) { FactoryBot.create(:topic) }

    before { get :show, params: { id: topic.id } }

    it 'has a 200 status code' do
      expect(response).to have_http_status(:ok)
    end

    it 'assigns @topic' do
      expect(assigns(:topic)).to eq topic
    end

    it 'renders the :show template' do
      expect(response).to render_template :show
    end
  end

  describe 'GET #edit' do
    let(:topic) { FactoryBot.create(:topic) }

    before do
      login_user(topic.user)
      get :edit, params: { id: topic.id }
    end

    it 'has a 200 status code' do
      expect(response).to have_http_status(:ok)
    end

    it 'assigns @topic' do
      expect(assigns(:topic)).to eq topic
    end

    it 'renders the :edit template' do
      expect(response).to render_template :edit
    end
  end

  describe 'PATCH #update' do
    let!(:topic) { FactoryBot.create(:topic) }
    let(:update_attributes) do
      {
        title: 'update title',
      }
    end

    before do
      login_user(topic.user)
    end

    it 'saves updated topic' do
      expect do
        patch :update, params: { id: topic.id, topic: update_attributes }
      end.to change(Topic, :count).by(0)
    end

    it 'updates updated topic' do
      patch :update, params: { id: topic.id, topic: update_attributes }
      topic.reload
      expect(topic.title).to eq update_attributes[:title]
    end

    it 'redirects the :create template' do
      patch :update, params: { id: topic.id, topic: update_attributes }
      topic = Topic.last
      expect(response).to redirect_to(topic_path(topic))
    end
  end

  describe 'DELETE #destroy' do
    let!(:topic) { FactoryBot.create(:topic) }

    before do
      login_user(topic.user)
    end

    it 'deletes the topic' do
      expect do
        delete :destroy, params: { id: topic.id }
      end.to change(Topic, :count).by(-1)
    end

    it 'redirects the root_path' do
      delete :destroy, params: { id: topic.id }
      expect(response).to redirect_to(root_path)
    end
  end
end
