require 'rails_helper'

RSpec.feature "CategoriesDestroy", type: :feature do
  let(:topic) { FactoryBot.create(:topic) }
  let!(:category) { FactoryBot.create(:category, user: topic.user, topic: topic) }
  let(:another_user) { FactoryBot.create(:user) }

  before do
    test_user_login(topic.user)
    visit topic_path(topic)
  end

  it 'カテゴリの削除リンクをクリックすると、カテゴリが削除され、スレッドの詳細ページにリダイレクトされる' do
    expect(page).to have_current_path topic_path(topic)
    expect do
      click_link '削除'
    end.to change(Category, :count).by(-1)
    expect(page).to have_current_path topic_path(topic)
  end

  it 'カテゴリを削除すると、「カテゴリを削除しました」とフラッシュメッセージが表示される' do
    click_link '削除'
    expect(page).to have_content 'カテゴリを削除しました'
  end

  it 'ログインしていない状態ではカテゴリの削除リンクが表示されない' do
    click_link 'ログアウト'
    visit topic_path(topic)
    expect(page).not_to have_content '削除'
  end

  it 'スレッド及びカテゴリの作成者以外のユーザーにはカテゴリの削除リンクが表示されない' do
    click_link 'ログアウト'
    test_user_login(another_user)
    visit topic_path(topic)
    expect(page).not_to have_content '削除'
  end

  it 'スレッドが削除されるとそのスレッド内のカテゴリも削除される' do
    expect do
      click_link 'スレッドを削除する'
    end.to change(Category, :count).by(-1)
  end
end
