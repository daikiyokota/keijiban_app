require 'rails_helper'

RSpec.feature 'UsersLogIn', type: :feature do
  let(:user) { FactoryBot.create(:user) }

  before { visit new_user_session_path }

  it 'ログインページで適切な値を入力するとログインに成功しroot_pathにリダイレクトされる' do
    expect(page).to have_current_path '/users/sign_in'
    test_user_login(user)
    expect(page).to have_current_path '/'
  end

  it 'ログインに成功すると「ログインしました。」とフラッシュメッセージが表示される' do
    test_user_login(user)
    expect(page).to have_content 'ログインしました。'
  end

  it 'ログインしている状態でログインページにアクセスしようとすると「すでにログインしています。」とフラッシュメッセージが表示される' do
    test_user_login(user)
    visit new_user_session_path
    expect(page).to have_content 'すでにログインしています。'
  end

  it 'ログインページでメールアドレスやパスワードの欄を空欄にするとエラーメッセージが表示される' do
    fill_in 'メールアドレス', with: ''
    fill_in 'パスワード', with: ''
    click_button 'ログイン'
    expect(page).to have_content 'メールアドレスまたはパスワードが違います。'
  end
end
