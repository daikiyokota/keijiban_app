require 'rails_helper'

RSpec.feature "CategoriesCreate", type: :feature do
  let(:topic) { FactoryBot.create(:topic) }
  let(:another_user) { FactoryBot.create(:user) }

  before do
    test_user_login(topic.user)
    visit topic_path(topic)
  end

  it 'スレッドの詳細ページでカテゴリ名を入力し追加ボタンを押すと、カテゴリが作成されスレッドの詳細ページにリダイレクトされる' do
    expect(page).to have_current_path topic_path(topic)
    expect do
      fill_in 'カテゴリ名', with: 'テストだよ'
      click_button 'カテゴリを追加する'
    end.to change(Category, :count).by(1)
    expect(page).to have_current_path topic_path(topic)
  end

  it 'カテゴリが作成されると、「カテゴリを追加しました」とフラッシュメッセージが表示される' do
    fill_in 'カテゴリ名', with: 'テストだよ'
    click_button 'カテゴリを追加する'
    expect(page).to have_content 'カテゴリを追加しました'
  end

  it 'ログインしていない状態ではカテゴリの作成フォームが表示されない' do
    click_link 'ログアウト'
    visit topic_path(topic)
    expect(page).not_to have_content 'カテゴリ名', 'カテゴリを追加する'
  end

  it 'スレッドの作成者以外のユーザーにはカテゴリの作成フォームが表示されない' do
    click_link 'ログアウト'
    test_user_login(another_user)
    visit topic_path(topic)
    expect(page).not_to have_content 'カテゴリ名', 'カテゴリを追加する'
  end
end
