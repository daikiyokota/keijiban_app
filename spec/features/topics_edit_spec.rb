require 'rails_helper'

RSpec.feature 'TopicsCreate', type: :feature do
  let(:user) { FactoryBot.create(:user) }
  let(:topic) { FactoryBot.create(:topic, user: user) }
  let(:another_topic) { FactoryBot.create(:topic) }

  before do
    test_user_login(user)
    visit edit_topic_path(topic)
  end

  it 'スレッドの編集に成功すると、詳細ページにリダイレクトされ、「スレッドを編集しました！」とフラッシュメッセージが表示される' do
    expect(page).to have_current_path edit_topic_path(topic)
    expect do
      fill_in 'タイトル', with: 'テストですよ'
      click_button '修正する'
    end.to change(Topic, :count).by(0)
    expect(page).to have_current_path topic_path(topic)
    expect(page).to have_content 'スレッドを編集しました！'
  end

  it '更新した情報がスレッドの詳細ページに反映されている' do
    fill_in 'タイトル', with: 'プログラミング始めた'
    click_button '修正する'
    expect(page).to have_current_path topic_path(topic)
    expect(page).to have_content 'プログラミング始めた'
  end

  it 'ログインしていない状態ではスレッドの編集ページにアクセスできない' do
    click_link 'ログアウト'
    visit edit_topic_path(topic)
    expect(page).to have_current_path "/users/sign_in"
  end

  it '他人が作ったスレッドの編集ページにアクセスしようとしてもroot_pathにリダイレクトされる' do
    visit edit_topic_path(another_topic)
    expect(page).to have_current_path root_path
  end
end
