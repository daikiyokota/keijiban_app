require 'rails_helper'

RSpec.feature "UsersSignUp", type: :feature do
  let(:user) { FactoryBot.create(:user) }

  before { visit new_user_registration_path }

  it '新規登録ページで適切な値を入力するとユーザー登録が完了しroot_pathにリダイレクトされる' do
    expect(page).to have_current_path '/users/sign_up'
    expect do
      fill_in 'メールアドレス', with: 'hogehoge@gmail.com'
      fill_in 'パスワード', with: 'hogehoge'
      fill_in '確認用パスワード', with: 'hogehoge'
      click_button '登録する'
    end.to change(User, :count).by(1)
    expect(page).to have_current_path '/'
  end

  it '新規登録ページでメールアドレスやパスワードの欄を空欄にするとエラーメッセージが表示される' do
    expect do
      fill_in 'メールアドレス', with: ''
      fill_in 'パスワード', with: ''
      fill_in '確認用パスワード', with: ''
      click_button '登録する'
    end.to change(User, :count).by(0)
    expect(page).to have_content 'メールアドレスを入力してください'
    expect(page).to have_content 'パスワードを入力してください'
  end

  it 'ログインしている状態で新規登録ページにアクセスしようとすると、root_pathにリダイレクトされ「すでにログインしています。」とメッセージが表示される' do
    test_user_login(user)
    visit new_user_registration_path
    expect(page).to have_content 'すでにログインしています。'
  end
end
