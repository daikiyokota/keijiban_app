require 'rails_helper'

RSpec.feature 'TopicsCreate', type: :feature do
  let(:user) { FactoryBot.create(:user) }

  before do
    test_user_login(user)
    visit new_topic_path
  end

  it '/topics/newで適切な値を入力するとスレッドが作成され、スレッドの詳細ページにリダイレクトされる' do
    expect(page).to have_current_path new_topic_path
    expect do
      fill_in 'タイトル', with: 'テストだよ'
      click_button '作成する'
    end.to change(Topic, :count).by(1)
    topic = Topic.last
    expect(page).to have_current_path topic_path(topic)
  end

  it 'スレッドが作成されると、「スレッドを作成しました！」とフラッシュメッセージが表示される' do
    fill_in 'タイトル', with: 'テストだよ'
    click_button '作成する'
    topic = Topic.last
    expect(page).to have_current_path topic_path(topic)
    expect(page).to have_content 'スレッドを作成しました！'
  end

  it '/topics/newでタイトルを空欄のまま作成しようとすると、「タイトルを入力してください」とフラッシュメッセージが表示される' do
    expect do
      fill_in 'タイトル', with: ''
      click_button '作成する'
    end.to change(Topic, :count).by(0)
    expect(page).to have_content 'タイトルを入力してください'
  end

  it 'ログインしていない状態ではスレッドの作成ページにアクセスできない' do
    click_link 'ログアウト'
    visit new_topic_path
    expect(page).to have_current_path "/users/sign_in"
  end
end
