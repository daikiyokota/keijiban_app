require 'rails_helper'

RSpec.feature 'TopicsSearch', type: :feature do
  let!(:topic) { FactoryBot.create(:topic, title: 'プログラミング始めました') }
  let!(:comment) { FactoryBot.create(:comment, content: '自粛期間です', topic: topic) }
  let!(:category) { FactoryBot.create(:category, name: '勉強期間', topic: topic, user: topic.user) }

  before do
    visit root_path
  end

  it 'タイトルに含まれている単語を入力し検索すると、その単語が含まれるスレッドが表示される' do
    fill_in 'search', with: 'プログラミング'
    click_button '検索'
    expect(page).to have_content 'プログラミング始めました'
  end

  it 'コメントに含まれている単語を入力し検索すると、そのコメントがスレッドのリンクと一緒に表示される' do
    fill_in 'search', with: '自粛期間'
    click_button '検索'
    expect(page).to have_content '自粛期間です', 'プログラミング始めました'
  end

  it 'カテゴリに含まれている単語を入力し検索すると、その単語を含むカテゴリがスレッドのリンクと一緒に表示される' do
    fill_in 'search', with: '勉強'
    click_button '検索'
    expect(page).to have_content '勉強期間', 'プログラミング始めました'
  end

  it 'スレッド、コメント、カテゴリのどれにも含まれていない単語で検索をかけると、スレッドは表示されず、「検索はヒットしませんでした」という文章が表示される' do
    fill_in 'search', with: 'Ruby'
    click_button '検索'
    expect(page).not_to have_content 'プログラミング始めました'
    expect(page).to have_content '検索はヒットしませんでした'
  end
end
