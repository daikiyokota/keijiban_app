require 'rails_helper'

RSpec.feature "CommentsCreate", type: :feature do
  let(:topic) { FactoryBot.create(:topic) }

  before do
    test_user_login(topic.user)
    visit topic_path(topic)
  end

  it 'スレッドの詳細ページで、コメントの本文を入力し投稿ボタンを押すと、コメントが作成され、スレッドの詳細ページにリダイレクトされる' do
    expect(page).to have_current_path topic_path(topic)
    expect do
      fill_in '本文', with: 'テストだよ'
      click_button '投稿する'
    end.to change(Comment, :count).by(1)
    expect(page).to have_current_path topic_path(topic)
  end

  it 'コメントが投稿されると「コメントを投稿しました」とフラッシュメッセージが表示される' do
    fill_in '本文', with: 'テストだよ'
    click_button '投稿する'
    expect(page).to have_content 'コメントを投稿しました'
  end

  it '本文を空欄のまま投稿しようとすると「本文を入力してください」とメッセージが表示される' do
    expect do
      fill_in '本文', with: ''
      click_button '投稿'
    end.to change(Comment, :count).by(0)
    expect(page).to have_content '本文を入力してください'
  end

  it 'ログインしていない状態ではコメントの投稿フォームが表示されない' do
    click_link 'ログアウト'
    visit topic_path(topic)
    expect(page).not_to have_content '本文', '投稿する'
  end
end
