require 'rails_helper'

RSpec.feature 'TopicsCreate', type: :feature do
  let(:user) { FactoryBot.create(:user) }
  let(:topic) { FactoryBot.create(:topic, user: user) }
  let(:another_topic) { FactoryBot.create(:topic) }

  before do
    test_user_login(user)
    visit topic_path(topic)
  end

  it 'スレッドの削除リンクをクリックするとそのスレッドを削除でき、root_pathにリダイレクトされる' do
    expect(page).to have_current_path topic_path(topic)
    expect do
      click_link 'スレッドを削除する'
    end.to change(Topic, :count).by(-1)
    expect(page).to have_current_path root_path
  end

  it 'スレッドを削除すると「スレッドを削除しました！」とフラッシュメッセージが表示される' do
    click_link 'スレッドを削除する'
    expect(page).to have_current_path root_path
    expect(page).to have_content 'スレッドを削除しました！'
  end

  it 'ログインしていない状態ではスレッドの削除リンクは表示されない' do
    click_link 'ログアウト'
    visit topic_path(topic)
    expect(page).not_to have_content 'スレッドを削除する'
  end

  it '他人の作成したスレッドの詳細ページでは削除リンクは表示されない' do
    visit topic_path(another_topic)
    expect(page).not_to have_content 'スレッドを削除する'
  end
end
