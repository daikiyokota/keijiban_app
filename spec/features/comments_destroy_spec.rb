require 'rails_helper'

RSpec.feature "CommentsDestroy", type: :feature do
  let(:comment) { FactoryBot.create(:comment) }
  let(:another_user) { FactoryBot.create(:user) }

  before do
    test_user_login(comment.user)
    visit topic_path(comment.topic)
  end

  it 'コメントを削除すると、そのコメントがあったスレッドの詳細ページにリダイレクトされる' do
    expect(page).to have_current_path topic_path(comment.topic)
    expect do
      click_link '削除する'
    end.to change(Comment, :count).by(-1)
    expect(page).to have_current_path topic_path(comment.topic)
  end

  it 'コメントを削除すると「コメントを削除しました」とフラッシュメッセージが表示される' do
    click_link '削除する'
    expect(page).to have_content 'コメントを削除しました'
  end

  it 'ログインしていない状態ではコメントの削除リンクが表示されない' do
    click_link 'ログアウト'
    visit topic_path(comment.topic)
    expect(page).not_to have_content '削除する'
  end

  it '自分以外のユーザーがしたコメントには削除リンクが表示されない' do
    click_link 'ログアウト'
    test_user_login(another_user)
    visit topic_path(comment.topic)
    expect(page).not_to have_content '削除する'
  end

  it 'スレッドが削除されるとそのスレッド内のコメントも削除される' do
    click_link 'ログアウト'
    test_user_login(comment.topic.user)
    visit topic_path(comment.topic)
    expect do
      click_link 'スレッドを削除する'
    end.to change(Comment, :count).by(-1)
  end
end
