require 'rails_helper'

RSpec.feature 'UsersLogOut', type: :feature do
  let(:user) { FactoryBot.create(:user) }

  before do
    test_user_login(user)
  end

  it 'ログアウトするとroot_pathにリダイレクトされる' do
    expect(page).to have_current_path '/'
    click_link 'ログアウト'
    expect(page).to have_current_path '/'
  end

  it 'ログアウトに成功すると「ログアウトしました。」とフラッシュメッセージが表示される' do
    click_link 'ログアウト'
    expect(page).to have_content 'ログアウトしました。'
  end

  it 'ログインしていない状態ではログアウトリンクは表示されない' do
    expect(page).to have_content 'ログアウト'
    click_link 'ログアウト'
    expect(page).not_to have_link 'ログアウト'
  end
end
