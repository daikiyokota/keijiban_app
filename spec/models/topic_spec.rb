require 'rails_helper'

RSpec.describe Topic, type: :model do
  it 'FactoryBotに定義したtopicは有効である' do
    topic = FactoryBot.build(:topic)
    expect(topic).to be_valid
  end

  it 'titleが空欄のtopicは無効である' do
    topic = FactoryBot.build(:topic, title: '')
    topic.valid?
    expect(topic.errors[:title]).to include('を入力してください')
  end

  it 'titleが31字以上のtopicは無効である' do
    topic = FactoryBot.build(:topic, title: 'a' * 31)
    topic.valid?
    expect(topic.errors[:title]).to include('は30文字以内で入力してください')
  end
end
