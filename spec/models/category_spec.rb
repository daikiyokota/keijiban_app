require 'rails_helper'

RSpec.describe Category, type: :model do
  it 'FactoryBotに定義したカテゴリは有効である' do
    category = FactoryBot.build(:category)
    expect(category).to be_valid
  end

  it '名前が空欄のカテゴリは無効である' do
    category = FactoryBot.build(:category, name: '')
    category.valid?
    expect(category.errors[:name]).to include('を入力してください')
  end

  it 'カテゴリ名が31字以上のカテゴリは無効である' do
    category = FactoryBot.build(:category, name: 'a' * 31)
    category.valid?
    expect(category.errors[:name]).to include('は30文字以内で入力してください')
  end

  it 'user_idがないカテゴリは無効である' do
    category = FactoryBot.build(:category, user_id: '')
    expect(category).not_to be_valid
  end

  it 'topic_idがないカテゴリは無効である' do
    category = FactoryBot.build(:category, topic_id: '')
    expect(category).not_to be_valid
  end
end
