require 'rails_helper'

RSpec.describe Comment, type: :model do
  it 'FactoryBotに定義したcommentは有効である' do
    comment = FactoryBot.build(:comment)
    expect(comment).to be_valid
  end

  it '本文が空欄のcommentは無効である' do
    comment = FactoryBot.build(:comment, content: '')
    comment.valid?
    expect(comment.errors[:content]).to include('を入力してください')
  end

  it '本文が101字以上のcommentは無効である' do
    comment = FactoryBot.build(:comment, content: 'a' * 101)
    comment.valid?
    expect(comment.errors[:content]).to include('は100文字以内で入力してください')
  end

  it 'user_idがないcommentは無効である' do
    comment = FactoryBot.build(:comment, user_id: '')
    expect(comment).not_to be_valid
  end

  it 'topic_idがないcommentは無効である' do
    comment = FactoryBot.build(:comment, topic_id: '')
    expect(comment).not_to be_valid
  end
end
