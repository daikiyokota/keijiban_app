require 'rails_helper'

RSpec.describe User, type: :model do
  it 'FactoryBotに定義したuserは有効である' do
    user = FactoryBot.build(:user)
    expect(user).to be_valid
  end

  it 'メールアドレスが空欄の場合は無効である' do
    user = FactoryBot.build(:user, email: '')
    user.valid?
    expect(user.errors[:email]).to include('を入力してください')
  end

  it '@がないメールアドレスの場合は無効である' do
    user = FactoryBot.build(:user, email: 'hogehogegmail.com')
    user.valid?
    expect(user.errors[:email]).to include('は不正な値です')
  end

  it '既に登録済みのメールアドレスの場合は無効である' do
    user = FactoryBot.create(:user)
    another_user = FactoryBot.build(:user, email: user.email)
    another_user.valid?
    expect(another_user.errors[:email]).to include('はすでに存在します')
  end

  it 'メールアドレスが51文字以上の場合は無効である' do
    user = FactoryBot.build(:user, email: 'a' * 50 + '@test.com')
    user.valid?
    expect(user.errors[:email]).to include('は50文字以内で入力してください')
  end

  it 'パスワードが空欄の場合は無効である' do
    user = FactoryBot.build(:user, password: '')
    user.valid?
    expect(user.errors[:password]).to include('を入力してください')
  end

  it 'パスワードが5文字以下の場合は無効である' do
    user = FactoryBot.build(:user, password: 'a' * 5)
    user.valid?
    expect(user.errors[:password]).to include('は6文字以上で入力してください')
  end

  it 'パスワードが129文字以上の場合は無効である' do
    user = FactoryBot.build(:user, password: 'a' * 129)
    user.valid?
    expect(user.errors[:password]).to include('は128文字以内で入力してください')
  end
end
