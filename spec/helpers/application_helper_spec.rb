require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  let(:base_title) { '掲示板!!' }

  describe "full_title_method" do
    context "full_title_method with page_title" do
      it "returns base_title with page_title" do
        expect(full_title('新規登録')).to eq "新規登録 - #{base_title}"
      end
    end

    context "full_title_method without page_title" do
      it "returns only base_title" do
        expect(full_title('')).to eq base_title
      end
    end

    context "full_title_method when page_title is nil" do
      it "returns only base_title" do
        expect(full_title(nil)).to eq base_title
      end
    end
  end
end
