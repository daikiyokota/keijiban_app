# README

掲示板!!は5ch掲示板のクローンアプリです。  
url: https://keijiban-yokotadaiki.herokuapp.com/


# Features

・新規登録機能  
・ログイン機能(mail,password)  
・パスワードの再設定機能  
・ユーザー情報の編集機能  
・スレッドの作成/編集/削除機能  
・カテゴリの追加/削除機能(スレッドの作成者のみ可能)  
・コメントの投稿/削除機能  
・検索機能(スレッドのタイトル、コメントの本文、カテゴリの名前に検索キーワードが含まれている場合抽出する)  

# Author

・作成者: 横田大記  
・E-mail: yd43star@gmail.com
