User.create!(
  email: "daiki1@gmail.coms",
  password: 'hogehoge',
  password_confirmation: 'hogehoge'
)

30.times do |n|
  Topic.create!(
    title: "プログラミング始めた件について#{n}",
    user_id: User.first.id
  )
  30.times do |m|
    Comment.create!(
      content: "コメント#{m}",
      user_id: User.first.id,
      topic_id: Topic.last.id
    )
  end
  3.times do |n|
    Category.create!(
      name: "カテゴリ#{n}",
      user_id: User.first.id,
      topic_id: Topic.last.id
    )
  end
end

