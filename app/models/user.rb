class User < ApplicationRecord
  validates :email, length: { maximum: 50 }
  validates :name, length: { maximum: 30 }
  has_many :topics, foreign_key: :user_id
  has_many :comments, foreign_key: :user_id
  has_many :categories, foreign_key: :user_id
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
end
