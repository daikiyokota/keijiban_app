class Topic < ApplicationRecord
  validates :title, presence: true, length: { maximum: 30 }
  belongs_to :user
  has_many :comments, foreign_key: :topic_id, dependent: :destroy
  has_many :categories, foreign_key: :topic_id, dependent: :destroy
end
