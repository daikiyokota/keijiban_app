class Category < ApplicationRecord
  belongs_to :topic
  belongs_to :user
  validates :name, presence: true, length: { maximum: 30 }
end
