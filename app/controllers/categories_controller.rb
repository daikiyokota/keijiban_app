class CategoriesController < ApplicationController
  before_action :authenticate_user!
  before_action :correct_user, only: [:destroy]

  def create
    @category = current_user.categories.build(category_params)
    @topic = @category.topic
    if @category.save
      flash[:success] = 'カテゴリを追加しました'
      redirect_to topic_path(@topic)
    else
      @comments = @topic.comments.page(params[:page]).per(100).includes([:user])
      @categories = @topic.categories
      @comment = Comment.new
      render template: 'topics/show'
    end
  end

  def destroy
    @category = Category.find(params[:id])
    @topic = @category.topic
    if @category.destroy
      flash[:success] = 'カテゴリを削除しました'
    end
    redirect_to topic_path(@topic)
  end

  private

  def category_params
    params.require(:category).permit(:name, :topic_id)
  end

  def correct_user
    @category = Category.find(params[:id])
    redirect_to(root_url) unless @category.user == current_user
  end
end
