class TopicsController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  before_action :correct_user, only: [:edit, :update, :destroy]
  def index
    @topics = Topic.all.order(created_at: :desc).page(params[:page]).per(10)
  end

  def search
    @q = params[:search]
    if @q.empty?
      @topics = Topic.all.order(created_at: :desc).page(params[:page]).per(10)
    else
      @topics = Topic.order(created_at: :desc).where("title LIKE ?", "%#{@q}%")
      @comments = Comment.where("content LIKE ?", "%#{@q}%").includes([:topic])
      @categories = Category.where("name LIKE ?", "%#{@q}%").includes(topic: :categories)
    end
  end

  def new
    @topic = Topic.new
  end

  def create
    @topic = current_user.topics.build(topic_params)
    if @topic.save
      flash[:success] = 'スレッドを作成しました！'
      redirect_to @topic
    else
      render 'new'
    end
  end

  def show
    @topic = Topic.find(params[:id])
    @comment = Comment.new
    @comments = @topic.comments.page(params[:page]).per(100).includes([:user])
    @category = Category.new
    @categories = @topic.categories
  end

  def edit
    @topic = Topic.find(params[:id])
  end

  def update
    @topic = Topic.find(params[:id])
    if @topic.update(topic_params)
      flash[:success] = 'スレッドを編集しました！'
      redirect_to @topic
    else
      render 'edit'
    end
  end

  def destroy
    @topic = Topic.find(params[:id])
    if @topic.destroy
      flash[:success] = 'スレッドを削除しました！'
    end
    redirect_to root_path
  end

  private

  def topic_params
    params.require(:topic).permit(:title)
  end

  def correct_user
    @topic = Topic.find(params[:id])
    redirect_to(root_url) unless @topic.user == current_user
  end
end
