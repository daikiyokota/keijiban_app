class CommentsController < ApplicationController
  before_action :authenticate_user!
  before_action :correct_user, only: [:destroy]
  def create
    @comment = current_user.comments.build(comment_params)
    @topic = @comment.topic
    if @comment.save
      flash[:success] = 'コメントを投稿しました'
      redirect_to topic_path(@topic)
    else
      @comments = @topic.comments.page(params[:page]).per(100).includes([:user])
      @category = Category.new
      @categories = @topic.categories
      render template: 'topics/show'
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    @topic = @comment.topic
    if @comment.destroy
      flash[:success] = 'コメントを削除しました'
    end
    redirect_to topic_path(@topic)
  end

  private

  def comment_params
    params.require(:comment).permit(:content, :topic_id)
  end

  def correct_user
    @comment = Comment.find(params[:id])
    redirect_to(root_url) unless @comment.user == current_user
  end
end
