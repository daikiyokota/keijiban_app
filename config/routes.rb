Rails.application.routes.draw do
  devise_for :users
  root 'topics#index'
  resources :topics do
    collection do
      get :search
    end
  end
  resources :comments, :only => [:create, :destroy]
  resources :categories, :only => [:create, :destroy]
  mount LetterOpenerWeb::Engine, at: "/letter_opener" if Rails.env.development?
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
